# VueCheatsheet

A cheatsheet for getting started with Vue.js

## Requirements 

- node / npm


## Commands 

### Install Vue CLI
```bash
$ npm install -g @vue/cli
```

### Upgrade Vue CLI
```bash
$ npm install -g @vue/cli --upgrade
```

### Setup Vue project with "Router" and "Sass loader"
```bash
$ vue create my-app
$ cd my-app
$ vue add router
$ npm install sass-loader node-sass
```

### Run Vue project
```bash
$ npm run serve
```
